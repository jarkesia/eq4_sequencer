void midiStart() {
  if (running) {
    return;
  }
  cli();
  resetSequencer();
  startSequencer(MASTER_MIDI);
  sei();
}


void midiContinue() {
  if (running) {
    return;
  }
  startSequencer(MASTER_MIDI);
}


void midiClock() {
  if ((master==MASTER_MIDI) && running) {

    if (pulseCounter & 1) {
      /*
        We shouldn't ever be here, clock pulse is still on.
        Might happen after tempo change etc.
        Should wait until the pulse drops.
      */

      OCR1B = TCNT1 + 1;  // This should trigger interrupt. Double triggering does not matter.

      while (pulseCounter & 1) {
        cli();
        updateShiftRegisters();
        sei();
      }

      while ((TCNT1 - OCR1B) < (OCR1B / 4)) {
        // Generate short off pulse.
        cli();
        updateShiftRegisters();
        sei();
      }
    }

    cli();

    if (pulseCounter > 0) {
      // Potential glitch if MIDI clock tempo stupdily slow and counter wraps over.
      OCR1B = max(TCNT1 / 3, MIN_BEAT_CLOCK_PULSE);  // We'll adjust pulse length slightly.
    }

    TCNT1 = 0;
    pulseCounter++;
    pulse();
    updateStepOut();
    sei();
  }
}


void midiStop() {
  if ((master==MASTER_MIDI) || !running) {
    return;
  }
  cli();
  stopSequencer();
  if (!(pulseCounter & 1)) {
    updateStepOut();
  }
  sei();
}


void midiSongPosition(int SPP) {

  /*
    At some point could optimize this a bit and make note where we are.
    Could then FF or RW from that point. But for now we will reset and
    FF sequencer to the correct position.
  */

  if (running) {
    // According to the spec.
    // Some sequencers might send MSP when running.
    return;
  }

  /*
    Will need to fast forward manually as we don't know
    how the feedback has been patched.

    Naturally if external signals have been somehow patched in,
    the state can not be fully consistent.

    Other tactic could be flashing yellow outputs through to figure
    out where the feedback (an other signals) are patched.
    Then emulate the sequence through internally.
  */

  cli();
  TIMSK1 = B00000000;
  resetSequencer();
  updateShiftRegisters();
  updateShiftRegisters();
  sei();

  unsigned long p = (SPP * 6UL);

  while (p > settings[SETTINGS_PULSES_PER_STEP]) {
    p = p - settings[SETTINGS_PULSES_PER_STEP];
    cli();
    advanceSequencer();
    updateShiftRegisters();
    updateShiftRegisters();
    /*
      If the patch is wild, updating registers twice is not enough.
      If the loop point to SR0 is straight from yellow jacks, two updates is enough.
      If it is from the black jacks, then we need more, depending how deep the loop is.
      Better approach would be to do it as long as they remain the same or something?
    */
    sei();
  }

  pulseCounter = p * 2;

  /*
    Optional peek mode that would make sense with step sequencers...
    For example SPP(3) @ 6 would jump to step 3 but the pulse would still be #18.
    Next pulse would not trigger stepping, next step would take place on pulse #25. 
  */

  if (pulseCounter == (settings[SETTINGS_PULSES_PER_STEP] * 2)) {
    advanceSequencer();
    pulseCounter = 0;  // This should skip first rising clock?
  }
}