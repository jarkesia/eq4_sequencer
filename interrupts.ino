ISR(TIMER0_COMPA_vect) {
  // NOT ENABLED
  //updateShiftRegisters();
}


/* COMP A when internal clock is used (master) */
ISR(TIMER1_COMPA_vect) {

  /*
    pulseCounter = 0 here only after start.
    When sequencer is playing it will be 1 after new step.
    We will step when it equals PULSES_PER_STEP when calling this function.
  */

  if (running) {
    pulseCounter++;
  }

  triggers &= ~(1 << settings[SETTINGS_RESET_OUT]);

  if (pulseCounter & 1) {
    if (running) {
      pulse();
    }
  } else {
    triggers &= ~(1 << settings[SETTINGS_CLOCK_OUT]);
  }

  /*
    We want step clock to be 50% so it could switch low on non even click.
  */

  updateStepOut();
  OCR1A = ocr1aPending;
  updateShiftRegisters(); // Forcing update will reduce jitter noise.
}


/*
  COMP B to generate falling clock when run on MIDI sync.
  pulseCounter shold always be non even when called.
*/
ISR(TIMER1_COMPB_vect) {
  triggers &= ~(1 << settings[SETTINGS_CLOCK_OUT]);

  if (pulseCounter & 1) {
    pulseCounter++;
  }

  updateStepOut();
}


void updateStepOut() {
   if ((pulseCounter > settings[SETTINGS_PULSES_PER_STEP]) || !running) {
    triggers &= ~(1 << settings[SETTINGS_STEP_OUT]);
  }  
}