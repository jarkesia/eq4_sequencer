void startSequencer(byte m) {

  if (totalPulseCount == 0) {
    sendRealTime(0xfa);  // START
  } else {
    sendRealTime(0xfb); // CONTINUE
  }

  running = true;
  triggers |= (1 << settings[SETTINGS_RUN_STOP_OUT]);
  updateShiftRegisters();  // To force a slight delay between run and first clock pulse...
  setMaster(m);

  // Only does something when MASTER_INT
  // Should not matter if rolls to the "negative" side...
  TCNT1 = OCR1A - RESET_PULSE;

  extReceived = false;

}


void stopSequencer() {
  sendRealTime(0xfc);  // STOP
  running = false;
  triggers &= ~(1 << settings[SETTINGS_RUN_STOP_OUT]);
}


void resetSequencer() {
  yellow = 1;
  counterA = 0;
  counterB = 0;
  blue = 0;
  red = 0;
  //triggers &= ~(1 << settings[SETTINGS_CLOCK_OUT]);
  triggers &= ~(1 << settings[SETTINGS_STEP_OUT]);
  triggers |= (1 << settings[SETTINGS_RESET_OUT]);
  pulseCounter = 0;
  totalPulseCount = 0;
  extReceived = false;
}


void advanceSequencer() {
  unsigned int y = yellow;

  yellow = yellow << 1;
  yellow |= (dsw & SR0) >> 6;
  yellow &= 0x3ff;

  y = y ^ yellow;

  triggers |= (1 << settings[SETTINGS_STEP_OUT]);

  if ((y & 0x200) && !(yellow & 0x200)) {
    // RTL will count on falling edge.
    counterB--;
    blue = (counterB & (overrideMask >> 5)) & B00011111;
  }

  counterA--;
  red = (counterA & (overrideMask >> 1)) & B00001111;
}



/*
  There are several approaches to count pulses when syncing.
  Half of them are wrong and half debatable. When using them
  in step sequencers, problems potentially do arise.

  Based on Roland din sync and MIDI sync that is based on it,
  first clock pulse received occures on first note of bar (or beat).

  After reset step sequencers are most likely already on beat 1,
  so they should most likely simply skip iterating on first pulse.

  If using MIDI SPP similar design problem arises. Should we take the
  sequencer one pulse ahead and then skip stepping on first pulse or not?

  Anyways if thinking of a situation where one step equals one MIDI beat,
  which is 6 pulses, then we should step on pulses 1, 7, 13 and so on...

*/

void pulse() {

  totalPulseCount++;

  triggers |= (1 << settings[SETTINGS_CLOCK_OUT]);

  if (pulseCounter > (settings[SETTINGS_PULSES_PER_STEP] * 2)) {
    if (running) {
      advanceSequencer();
    }
    pulseCounter = 1;
  }

  sendRealTime(0xf8);

}


void sendRealTime(byte b) {
  if (settings[SETTINGS_BEAT_CLOCK_OUT]) {
    MIDI.sendRealTime(b);
  }  
}
