#include <SPI.h>
#include <EEPROMex.h>
#include <MIDI.h>

/*
    Firmware for EQ4-SEQ-DIR.
    Copyright (C) 2020-2024  Jari Suominen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 Set the fuses of the ATMEGA328 like so:
 avrdude -P /dev/ttyACM0 -c avrispmkII -p m328p -U lfuse:w:0xff:m -U hfuse:w:0xd7:m -U efuse:w:0xff:m
*/

inline void latchOutputs() __attribute__((always_inline));
inline void pulse() __attribute__((always_inline));
inline void updateShiftRegisters() __attribute__((always_inline));
inline void updateStepOut() __attribute__((always_inline));
inline bool updatePot(bool force) __attribute__((always_inline));
inline void updateInternalClockTempo() __attribute__((always_inline));
inline void sendRealTime(byte b) __attribute__((always_inline));
inline void setMaster(byte m) __attribute__((always_inline));
inline void advanceSequencer() __attribute__((always_inline));
inline void startSequencer() __attribute__((always_inline));
inline void resetSequencer() __attribute__((always_inline));


#define SETTINGS_CLOCK_OUT 0
#define SETTINGS_RUN_STOP_OUT 1
#define SETTINGS_RESET_OUT 2
#define SETTINGS_STEP_OUT 3

#define SETTINGS_CLOCK_IN 4
#define SETTINGS_RUN_STOP_IN 5

#define SETTINGS_PULSES_PER_STEP 6  // 0010
#define SETTINGS_PULSE_PRESCALER 7
#define SETTINGS_DSW 8
#define SETTINGS_TEMPO_POT_CURVE 9

#define SETTINGS_BEAT_CLOCK_IN 10
#define SETTINGS_BEAT_CLOCK_OUT 11
#define SETTINGS_MIDI_THRU_MODE 12
#define SETTINGS_STROBE_OUTPUTS 13

#define SETTINGS_SETTING 14

#define SETTINGS_COUNT SETTINGS_SETTING + 1
byte settings[SETTINGS_COUNT] = {
  4,  // CLOCK OUT
  5,  // RUN STOP OUT
  6,  // RESET OUT
  7,  // STEP OUT
  0,  // CLOCK IN

  1,          // RUN STOP IN
  6,          // PULSES PER STEP
  2,          // PULSE PRESCALER
  B00001100,  // DSW
  B00000001,  // TEMPO POT CURVE

  0,  // MIDI BEAT CLOCK IN
  0,  // MIDI BEAT CLOCK OUT
  0,  // MIDI THRU MODE
  0,  // STROBE OUTPUTS
  0,  // SETTING
};

byte lsb[32];  // For setting edit turning numerical parameters to LSB

boolean settingEdit = false;
unsigned long settingHysteresis = 0;
#define SETTING_ENTER_ITERATIONS 200000

#define PPQ 24
#define RESET_PULSE 2250          // 9ms @ 64, Roland 808 specs.
#define MIN_BEAT_CLOCK_PULSE 200  // 1ms @ 64
unsigned long int totalPulseCount = 0;
bool running = false;
byte pulseCounter = 0;
byte tempo = 0;
unsigned int ocr1aPending = 0;
byte prescaler = 2;

byte counterA = 0;
byte counterB = 0;

/* Jacks and buttons */
unsigned int yellow = 0;  // Main shift register
byte red = 0;             // 4-bit counter
byte blue = 0;            // 5-bit counter
byte black = 0;           // DSW outputs
byte dsw = 0;             // DSW select (red)
#define SR0 B01000000
#define RST B10000000
byte green = 0;  // DSW input

/*
  Bit 0 = disabled, Bits 1 => physical jacks.
 */
unsigned int overrideMask = ~0;
unsigned int triggers = 0;
unsigned int preIn = 0;

#define BUTTON_GREEN B00000100
#define BUTTON_WHITE B00001000

#define MASTER_INT 0
#define MASTER_EXT 1
#define MASTER_MIDI 2
byte master = MASTER_INT;
bool extReceived = false;  // To check if external clock is detected when running.

MIDI_CREATE_DEFAULT_INSTANCE();

int settingsEEPROM;
const int maxAllowedWrites = 80;  // ???
const int memBase = 350;          // ???


void setup() {

  SPI.setClockDivider(SPI_CLOCK_DIV2);
  SPI.begin();

  // EEPROMex:
  EEPROM.setMemPool(memBase, 2048);
  EEPROM.setMaxAllowedWrites(maxAllowedWrites);
  delay(100);

  byte settingsNAN = EEPROM.getAddress(sizeof(byte));
  settingsEEPROM = EEPROM.getAddress(sizeof(byte) * SETTINGS_COUNT);

  if (EEPROM.readByte(settingsNAN) != 0xFF) {
    EEPROM.readBlock<byte>(settingsEEPROM, settings, SETTINGS_COUNT);
  } else {
    // Fresh chip, EEPROM does not contain any data.
    EEPROM.updateByte(settingsNAN, 0);
    EEPROM.updateBlock<byte>(settingsEEPROM, settings, SETTINGS_COUNT);
  }

  for (int i = 0; i < 32; i++) {
    lsb[i] = 0 | (((i & 16) >> 4) | ((i & 8) >> 2) | (i & 4) | ((i & 2) << 2) | ((i & 1) << 4));
  }

  /* Timer 0 8-bit */
  /* Let's switch the interrupt of, which will disable ms etc Arduino core features. */
  TCCR0A = B00000010;
  TCCR0B = B10000010;  // prescaler @ 8
  //TIMSK0 = B00000010;  // enable A
  TIMSK0 = 0;   // Disable interrupts
  OCR0A = 200;  // 1ms

  /* Timer 1 16-bit */
  TCCR1A = B00000000;
  TCCR1B = B00001011;   // prescaler @ 64
  TIMSK1 = B00000010;   // enable comp A
  OCR1A = 100;          // smallish isignificant value
  OCR1B = RESET_PULSE;  // 9ms

  tempo = 0;  // This should force tempo pot read

  DDRB = B00101111;
  PORTB = B00000000;

  DDRC = B00000000;
  PORTC = B00000000;

  DDRD = B10000010;
  PORTD = B00001100;

  resetSequencer();

  /* Analog read */
  ADMUX = (DEFAULT << 6) | (B00000101);  // Enable analog read on pin 5
  ADCSRB = B01000001;
  ADCSRA |= (1 << ADSC);

  while (!updatePot(true)) {};
  updateInternalClockTempo();

  MIDI.begin(MIDI_CHANNEL_OMNI);
  applySettings();
}


void loop() {

  if (settingEdit) {
    if (settings[SETTINGS_SETTING] < 10) {
      yellow = 0x200 >> settings[SETTINGS_SETTING];
    } else {
      yellow = (~(0x200 >> (settings[SETTINGS_SETTING] - 10)) & 0x3ff);
    }
    switch (settings[SETTINGS_SETTING]) {
      case SETTINGS_DSW:
      case SETTINGS_TEMPO_POT_CURVE:
      case SETTINGS_PULSE_PRESCALER:
        blue = settings[settings[SETTINGS_SETTING]] & B00011111;
        break;
      default:
        blue = lsb[settings[settings[SETTINGS_SETTING]] & B00011111];
        break;
    }

    red = B00001111;

    cli();
    PORTB |= B00000001;
    PORTB &= B11111001;
    green = SPI.transfer(yellow);
    dsw = SPI.transfer(((yellow >> 8) & B00000011) | (red << 2) | (blue << 6));
    SPI.transfer((blue >> 2) | (black << 3));
    PORTB |= B00000110;
    PORTB &= B11111110;
    sei();

    updatePot(false);
    settings[SETTINGS_SETTING] = map(tempo, 0, 255, SETTINGS_SETTING - 1, 0);  // tempo last setting

    switch (settings[SETTINGS_SETTING]) {
      case SETTINGS_BEAT_CLOCK_IN:
      case SETTINGS_BEAT_CLOCK_OUT:
      case SETTINGS_MIDI_THRU_MODE:
      case SETTINGS_STROBE_OUTPUTS:
        // On - Off settings will turn all bits on/off when tweaked.
        if (green & B00011111) {
          settings[settings[SETTINGS_SETTING]] = B00011111;
        } else if (dsw & B00011111) {
          settings[settings[SETTINGS_SETTING]] = 0;
        }
        break;
      case SETTINGS_TEMPO_POT_CURVE:
        if (green & B00011111) {  // 6 possible values.
          settings[settings[SETTINGS_SETTING]] = green & B00011111;
        } else if (dsw & B00011111) {
          settings[settings[SETTINGS_SETTING]] = 0;
        }
        break;
      case SETTINGS_PULSE_PRESCALER:
        if (green & B00011111) {  // 5 possible values.
          settings[settings[SETTINGS_SETTING]] = green & B00011111;
        }
        break;
      case SETTINGS_DSW:
        settings[settings[SETTINGS_SETTING]] |= green & B00011111;
        settings[settings[SETTINGS_SETTING]] &= ~(dsw & B00011111);
        break;
      default:
        settings[settings[SETTINGS_SETTING]] |= lsb[(green & B00011111)];
        settings[settings[SETTINGS_SETTING]] &= ~lsb[(dsw & B00011111)];
        break;
    }

    if (!(PIND & BUTTON_WHITE)) {
      if (settingHysteresis == 0) {
        settingEdit = false;
        cli();
        applySettings();
        resetSequencer();
        updateShiftRegisters();
        updateShiftRegisters();
        updateInternalClockTempo();
        preIn = 0;
        sei();
      }
      settingHysteresis = 1000;
    } else {
      if (settingHysteresis > 0) settingHysteresis--;
    }

    return;
  }

  while (MIDI.read()) {}

  /* Buttons */
  if (!(PIND & BUTTON_GREEN)) {
    if (!running) {
      cli();
      if (master == MASTER_EXT) {
        startSequencer(MASTER_EXT);
      } else {
        startSequencer(MASTER_INT);
      }
      sei();
    }
  } else if (!(PIND & BUTTON_WHITE)) {
    cli();
    if (running) {  // We will allow stopping even when running on slave on MIDI sync.
      stopSequencer();
    }
    if (settingHysteresis == 0) {
      if (!extReceived) {
        setMaster(MASTER_INT);
      }
      TCCR1B = B00001011;  // prescaler @ 64 CTC
      TIMSK1 = B00000010;  // enable comp A
      TCNT1 = OCR1A - RESET_PULSE;
      resetSequencer();
    }
    sei();
    settingHysteresis++;
  } else {
    if (settingHysteresis > 1000) settingHysteresis = 1000;
    if (settingHysteresis > 0) settingHysteresis--;
  }

  if (settingHysteresis > SETTING_ENTER_ITERATIONS) {
    settingEdit = true;
    settingHysteresis = 1000;
    TIMSK1 = 0;  // timers out.
    yellow = 0x3ff;
    while (!(PIND & BUTTON_WHITE)) {
      cli();
      updateShiftRegisters();
      sei();
    }
    return;
  }

  int i = ((green & B00011111) | (dsw << 5)) << 1;
  int j = preIn ^ i;
  /* Inputs */

  //if (i & (RST << 5)) { // blocking
  if (j & (RST << 6) && (i & (RST << 6))) {  // rising
    /*
      Original behaviour is blocking, but I guess for compatibility,
      should do rising trigger instead (so would work as eurorack RESET as well)
    */
    byte b = pulseCounter & 1;
    resetSequencer();
    pulseCounter = b;
    if (pulseCounter == 1) {
      triggers |= (1 << settings[SETTINGS_STEP_OUT]);
    }
  }

  if ((settings[SETTINGS_RUN_STOP_IN] != 0xff) && (j & (1 << settings[SETTINGS_RUN_STOP_IN]))) {
    if (i & (1 << settings[SETTINGS_RUN_STOP_IN])) {
      if (!running) {
        startSequencer(MASTER_INT);
      }
    } else {
      if (running) {
        cli();
        stopSequencer();
        sei();
      }
    }
  }

  if (j & (1 << settings[SETTINGS_CLOCK_IN])) {
    extReceived = true;
    if (running && (master != MASTER_MIDI)) {
      pulseCounter++;
      triggers &= ~(1 << settings[SETTINGS_RESET_OUT]);
      if (i & (1 << settings[SETTINGS_CLOCK_IN])) {
        if (master == MASTER_INT) {
          cli();
          setMaster(MASTER_EXT);
          sei();
        }
        pulse();
      } else {
        triggers &= ~(1 << settings[SETTINGS_CLOCK_OUT]);
      }
      updateStepOut();
    }
  }

  preIn = i;

  cli();
  updateShiftRegisters();
  sei();

  if (updatePot(false)) {
    updateInternalClockTempo();
  }
}


byte debug = 0;
void updateShiftRegisters() {
  /*
    After pulse we anyway need to refresh everything twice before
    hitting the correct state. So might as well read and write
    simultanously as this should double the speed.

    If shift didn't take place before calling this, one iteration
    should be enough for update everything in correct state.

    If shift was called before, then we need to roll this twice.
    After first run, black outputs will be off, after second,
    they also should be set correctly.

  */

  // Close latch of the 74HC165

  PORTB |= B00000001;

  PORTB &= B11111001;

  byte r;
  byte b;

  if ((pulseCounter == 0) && (settings[SETTINGS_STROBE_OUTPUTS]) && running) {
    // We only strobe shift register and counter outputs
    r = (triggers >> 1) & B00001111;
    b = (triggers >> 5) & B00011111;
    green = SPI.transfer(0);
    dsw = SPI.transfer((r << 2) | (b << 6));
  } else {
    r = (red | (triggers >> 1)) & B00001111;
    b = (blue | (triggers >> 5)) & B00011111;
    green = SPI.transfer(yellow);
    dsw = SPI.transfer((yellow >> 8) | (r << 2) | (b << 6));
  }

  /*
    Missing DSW board should implement this?
    Blue/green might need to be reversed.
    Colors on original Quartet:
      pink wire to red plugs
      blue 0 + 1  = b/y
      blue 2 + 3  = r/y
      green 0 + 1 = r/y
      green 2 + 3 = b/y
    Pinout of the original (missing) DSW PCB and the reversed cables at the patchbay
    could indicate that the default input is flipped on two channels.
    Need to check the wiring colors, now assume that red/yellow = blue and blue/yellow to green (panel).
  */

  dsw ^= settings[SETTINGS_DSW];
  black = (dsw & green) | (~dsw & b);

  black = (black & (overrideMask >> 10)) | (triggers >> 10);

  SPI.transfer((b >> 2) | (black << 3));

  PORTB |= B00000110;

  // Open latch of the 74HC165
  PORTB &= B11111110;
}


void applySettings() {

  /* Making sure everything is within correct range. */
  for (int i = 0; i < SETTINGS_COUNT; i++) {
    switch (i) {
      case SETTINGS_CLOCK_OUT:
      case SETTINGS_RUN_STOP_OUT:
      case SETTINGS_RESET_OUT:
      case SETTINGS_STEP_OUT:
        if (settings[i] > 14) {
          settings[i] = 0;
        }
        break;
      case SETTINGS_CLOCK_IN:
      case SETTINGS_RUN_STOP_IN:
        if (settings[i] > 10) {
          settings[i] = 0;
        }
        break;
    }
  }

  switch (settings[SETTINGS_PULSE_PRESCALER]) {
    case 16:
      prescaler = 1;
      TCCR1B = B00001011;
      break;
    case 4:
      prescaler = 4;
      TCCR1B = B00001011;
      break;
    case 2:
      prescaler = 2;
      TCCR1B = B00001100;
      break;
    case 1:
      prescaler = 4;
      TCCR1B = B00001100;
      break;
    case 8:
    default:
      prescaler = 2;
      TCCR1B = B00001011;
      break;
  }

  overrideMask = ~((1 << settings[SETTINGS_CLOCK_OUT]) | (1 << settings[SETTINGS_RUN_STOP_OUT]) | (1 << settings[SETTINGS_RESET_OUT]) | (1 << settings[SETTINGS_STEP_OUT]));

  //MIDI.setInputChannel(settings[SETTINGS_MIDI_IN] + 1);

  if (settings[SETTINGS_BEAT_CLOCK_IN]) {
    MIDI.setHandleClock(midiClock);
    MIDI.setHandleStart(midiStart);
    MIDI.setHandleContinue(midiContinue);
    MIDI.setHandleStop(midiStop);
    MIDI.setHandleSongPosition(midiSongPosition);
  } else {
    MIDI.setHandleClock(nullptr);
    MIDI.setHandleStart(nullptr);
    MIDI.setHandleContinue(nullptr);
    MIDI.setHandleStop(nullptr);
    MIDI.setHandleSongPosition(nullptr);
  }

  MIDI.setThruFilterMode(settings[SETTINGS_MIDI_THRU_MODE] & 1);

  EEPROM.updateBlock<byte>(settingsEEPROM, settings, SETTINGS_COUNT);
}


void setMaster(byte m) {
  switch (m) {
    case MASTER_INT:
      {
        master = true;
        TCCR1B |= B00001000; // enable CTC
        TIMSK1 = B00000010;  // enable comp A
        break;
      }
    case MASTER_EXT:
      {
        TIMSK1 = B00000000;  // Timer 1 interrupts off.
        break;
      }
    case MASTER_MIDI:
      {
        master = false;
        TCCR1B &= B11110111;  // disable CTC
        TIMSK1 = B00000100;   // enable comp B
        break;
      }
    default:
      break;
  }
  master = m;
  TCNT1 = 0;
  TIFR1 = 0;  // Clear timer 1 interrupt flags
}


bool updatePot(bool force) {
  if (!(ADCSRA & (1 << ADSC))) {
    /*
      analogRead() is blocking using internal while loop.
      As we want to run everything as swiftly as possible
      we let the ADC do its thing in the background
      and only update the results when reading is finished.
    */

    int p = ADC;  // Compiler should handle ADCL and ADCH merge when using this.

    if ((abs(tempo * 4 - p) > 3) || force) {
      tempo = p / 4;
      return true;
    }

    ADCSRA |= (1 << ADSC);  // start new ADC conversion
    return false;
  }
}


void updateInternalClockTempo() {

  // stock scale would be from 77 to 2163.
  // From 4BPS to 100BPS would be from 104 to 2604.
  // Could consider calculating a uint[256] array when applying settings and just use
  // lookup table here.

  switch (settings[SETTINGS_TEMPO_POT_CURVE]) {
    case 16:
      /*
        Logarithmic original.
        This is original range, slightly more exponential.
        There is bigger sector for audio frequencies than on stock,
        but mho could be more.
      */
      ocr1aPending = (76 + (int)(2087 * pow((1.0 * (255 - tempo)) / 255.0, 1.6))) * prescaler;
      break;
    case 8:
      /*
        Logarithimic original with lower speeds
        Will stretch the low end to slower speeds, 37BPM from Roland specs.
      */
      ocr1aPending = (76 + (int)(4146 * pow((1.0 * (255 - tempo)) / 255.0, 1.95))) * prescaler;
      break;
    case 4:
      ocr1aPending = (76 + (255 - tempo)) * 2;  // From 12AM up
      break;
    case 2:
      /* Roland TR-909 specs, 37BPM to 290BPM */
      ocr1aPending = (250000.0 / ((constrain(tempo, 1, 254) + 36) * 12.0 / 60.0)) * prescaler;
      break;
    case 1:
      /* User defined? with sysex chart? */
    default:
      /*
        This gives more or less exact responce the real EQ4 has,
        from ~7.4ms to ~208ms / step.
        Problem is that the audio frequencies could have more resolution.
      */
      ocr1aPending = map(tempo, 0, 255, 2163, 76) * prescaler;
      break;
  }

}